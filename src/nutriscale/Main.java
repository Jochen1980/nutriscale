package nutriscale;

import java.util.Scanner;

import javax.swing.JOptionPane;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import nutriscale.model.NutriScale;
import nutriscale.model.NutriScaleModel;
import nutriscale.tests.*;

public class Main {

	/* Text user interface. */
	public static void showTextUserInterface(){
		System.out.println("=====================================================");
		System.out.println("*** NutriScale ***");
		System.out.println("Anwendungsfaelle:");
		System.out.println("0 - Anwendung beenden.");
		System.out.println("1 - NutriScale mit Standardkonfiguration starten.");
		System.out.println("9 - Testfälle ausführen.");
		boolean quit = false;
		while (!quit) {
			Scanner scanner = new Scanner(System.in);
			System.out.print("Eingabe: ");
			String eingabe = scanner.nextLine();
			if (eingabe.equals("0")) {
				quit = true;
			} else if (eingabe.equals("1")) {
				System.out.println("NutriScale - Demo");
				NutriScaleModel nsm = new NutriScaleModel();
				NutriScale ns = nsm.getNutriScale();
				String res = ns.getSummary();
				System.out.println(res);
			} else if (eingabe.equals("9")) {
				System.out.println("NutriScale - Tests");
				Result result = JUnitCore.runClasses(NutriScaleModelTest.class);
				String message = "Ran: " + result.getRunCount() + ", Ignored: " + result.getIgnoreCount() + ", Failed: " + result.getFailureCount();
				//JOptionPane.showMessageDialog(null, message, "JUnit Tests NutriScale", JOptionPane.WARNING_MESSAGE);
				System.out.println("JUnit Tests NutriScale: " + message);
			} else {
			}
		}
		System.out.println("=== NutriScale - END ===");
	}
	
	// Run As ... Java Application
	public static void main(String[] args) {
		showTextUserInterface();
	}

}
