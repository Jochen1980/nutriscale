package nutriscale.tests;

import static org.junit.Assert.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import nutriscale.model.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NutriScaleModelTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void doNutriScaleTest(){
		// fail("Not yet implemented!");
		DgePyramid pyramid = new DgePyramid();
		Date mealDate = new Date();
		FoodItemGroup seelachs1Group = new FoodItemGroup(FoodItemGroupId.FISCH, pyramid);
		FoodItem seelachs1 = new FoodItem("Seelachs", seelachs1Group,  100.0, mealDate, 250.0, FoodUnit.G);
		FoodItemGroup kartoffel1Group = new FoodItemGroup(FoodItemGroupId.KARTOFFELN, pyramid);
		FoodItem kartoffeln1 = new FoodItem("Kartoffeln", kartoffel1Group, 0.0, mealDate, 300.0, FoodUnit.G); 
		ArrayList<FoodItem> items = new ArrayList<FoodItem>();
		items.add(seelachs1);
		items.add(kartoffeln1);
		Meal meal = new Meal("Seelachs mit Kartoffeln", items);
		NutriScale ns1 = new NutriScale(meal, pyramid);      
		Double exp0 = 7.2727272727272725;
		//Double act0 = Double.valueOf(df.format(ns1.getNutriScore()));
		Double act0 = ns1.getNutriScore();
		assertEquals("doNutriScaleTest-0", exp0, act0); // check Score
		int act1 = ns1.getNutriRatio(); // check Ratio
		int exp1 = 5;
		assertEquals("doNutriScaleTest-1", exp1, act1);
	}
	
}
