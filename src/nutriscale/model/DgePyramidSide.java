package nutriscale.model;

import java.util.List;

/*
 * One side of the pyramid consists of a list of groups of groceries.
 */
public class DgePyramidSide {
	
	private String name; 
	private List<FoodItemGroup> groups;
	
	public DgePyramidSide(String name, List<FoodItemGroup> groups){
		this.name = name;
		this.groups = groups;
	}
	
	public List<FoodItemGroup> getGroups(){
		return this.groups;
	}
}