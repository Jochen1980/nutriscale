package nutriscale.model;

import java.util.ArrayList;
import java.util.Date;


public class NutriScaleModel {
	
	private NutriScale nutriScale;

	public NutriScaleModel(){
		// Demo anlegen - Kartoffel, Seelachs-Filet
		DgePyramid pyramid = new DgePyramid();
		Date mealDate = new Date();
		FoodItemGroup seelachs1Group = new FoodItemGroup(FoodItemGroupId.FISCH, pyramid);
		FoodItem seelachs1 = new FoodItem("Seelachs", seelachs1Group,  100.0, mealDate, 250.0, FoodUnit.G);
		FoodItemGroup kartoffel1Group = new FoodItemGroup(FoodItemGroupId.KARTOFFELN, pyramid);
		FoodItem kartoffeln1 = new FoodItem("Kartoffeln", kartoffel1Group, 0.0, mealDate, 300.0, FoodUnit.G); 
		ArrayList<FoodItem> items = new ArrayList<FoodItem>();
		items.add(seelachs1);
		items.add(kartoffeln1);
		Meal meal = new Meal("Seelachs mit Kartoffeln", items);
		nutriScale = new NutriScale(meal, pyramid);
	}
	
	public NutriScale getNutriScale(){
		return this.nutriScale;
	}
	
}
