package nutriscale.model;

import java.util.Date;

/*
 * Class represents one grocery.
 */
public class FoodItem {
	
	String name;
	FoodItemGroup group; 
	Double ratioAnimalVegetable; 	// 100 % = only animal fibers; 
	Double weight; 					// in g 
	FoodUnit unit;
	Date mealtime;
	
	public FoodItem(String name, FoodItemGroup group, Double ratioAnimalVegetable, Date mealtime, Double weight, FoodUnit unit){
		this.name = name;
		this.group = group;
		this.ratioAnimalVegetable = ratioAnimalVegetable;
		this.weight = weight;
		this.unit = unit;
	}
	
	public FoodItemGroup getFoodItemGroup(){
		return this.group;
	}
	
	public Double getWeight(){
		return this.weight;
	}
	
	public FoodUnit getUnit(){
		return this.unit;
	}
	
	public Double getRatioAnimalVegetable(){
		return this.ratioAnimalVegetable;
	}
	
}
