package nutriscale.model;

import java.util.List;

public class Meal {
	
	private String name;
	private List<FoodItem> foodItems;
	
	public Meal(String name, List<FoodItem> foodItems){
		this.name = name;
		this.foodItems = foodItems;
	}
	
	public List<FoodItem> getFoodItems(){
		return this.foodItems;
	}
	
	public String getName(){
		return this.name;
	}
}
