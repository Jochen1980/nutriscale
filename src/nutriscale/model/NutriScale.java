package nutriscale.model;

/**
 * NutriScale liefert fuer eine Mahlzeit eine qualitative und quantitive Qualitätsaussage in Anlehnung an den Empfehlungen der DGE. 
 * 
 * Der NutriScore beschreibt durch einen Wert von 0 bis 10 die Qualität der Mahlzeit. Hierfuer werden die Lebensmittel der Mahlzeit 
 * auf der DGE-Lebensmittelpyramide einsortiert und mit einem Wert versehen. 10 entspricht dem qualitativ besten Wert.
 * Die NutriRatio beschreibt durch einen Wert von 0 bis 10 die Uebereinstimmung des Nahrungsmittelverhaeltnisses mit der empfohlenen
 * Nahrungszusammenstellung (75 % der Nahrung sollte aus pflanzlichen Lebensmitteln stammen).  
 * 
 * Ausblick:
 * Die Berechnung sollte auch fuer eine Liste der Nahrungsmittel moeglich sein.
 * Die 
 */
public class NutriScale {

	private Double nutriScore;
	private int nutriRatio;
	private Meal meal;
	
	public NutriScale(Meal meal, DgePyramid pyramid){ // Konstruktor
		// TODO considering uses interfaces for different pyramids.
		this.meal = meal;
		
		// Eingabeparameter:
		// - ein Lebensmitteleintrag kann mit einem DGE-Punktewert zwischen 1 und 10 klassifiziert werden.
		// - fuer einen Lebensmitteleintrag ist bekannt, zu welchen Teilen er pflanzlichen oder tierischen Ursprungs ist. 
		// - verschiedene Lebensmittel (Food) werden zu einem Menue (Meal) zusammengefasst.
		// - verschiedene Menues bilden die Tagesaufnahme an Energie
		// - die Tagesaufnahme kann mit den Zielen der Person bilanziert werden.
		
		// Berechnungen:
		nutriScore = computeNutriScore(meal, pyramid);
		nutriRatio = computeNutriRatio(meal);
		
		// Ausblick - Moegliche Optimierungen:
		// - die vier Bereiche der Pyramide berücksichtigen, statt nur die beiden Gruppen 'tierisch' und 'pflanzlich'
	}
	
	public Double getNutriScore(){ 
		return nutriScore;
	}
	
	public int getNutriRatio(){
		return nutriRatio;
	}
	
	public String getSummary(){
		String res = "";
		res += "Mahlzeit: " + this.meal.getName() + "\n";
		res += "NutriScaleScore: " + nutriScore.toString() + "\n";
		res += "NutriScaleRatio: " + nutriRatio + "\n";
		return res;
	}
	
	private Double computeNutriScore(Meal meal, DgePyramid pyramid){
		/*
		 * Ernaehrungspunktzahl:
		 * 
		 *       (x Gramm Lebensmittel A * Punktzahl) + (y Gramm Lebensmittel B * Punktzahl)
		 *  res = --------------------------------------------------------------------------- 
		 *                       (x Gramm Lebensmittel A + y g Lebensmittel B)
		 * 
		 * Erlaeuterung:
		 * Jedes Lebensmittel wird mit einem Punktewert gemaess der DGE-Lebensmittelpyramide versehen.
		 * Jede Seite der Lebensmittelpyramide wird klassifiziert in Werten mit 1 (schlecht, Pyramidenspitze bis 10 fuer sehr gut, Pyramidenseitenboden)
		 * 
		 *  Beispiel
		 *  Name Menge Pflanzlich Tierisch
		 *  
		 */
		
		// Eingabe ...
		// Mahlzeit und Pyramide erlauben die Berechnung.
		
		// Beispiel:
		// ein tolles Lebensmittel mit 9 Punkten in der Mahlzeit
		// 400 g Blattsalat ... 
		// 400 * 10 / 400 = 10
		
		Double numerator = 0.0; // Zaehler
		Double denominator = 0.0; // Nenner
		for (FoodItem item : meal.getFoodItems()){
			FoodItemGroup foodItemGroup = item.getFoodItemGroup();
			int foodItemScore = foodItemGroup.getScore(); 
			Double weightOfItem = item.getWeight();
			FoodUnit unit = item.getUnit();
			// Annahme Gewicht ist stets in Gramm
			// TODO weightOfItem noch in Anlehnung an andere Einheiten anpassen
			Double sumToAdd = 0.0;
			sumToAdd += weightOfItem * foodItemScore;
			numerator += sumToAdd;
			denominator += weightOfItem;
		}
		Double res = numerator / denominator;
		//Double res = 0.1;
		return res;
	}
	
	private int computeNutriRatio(Meal meal){
		/*
		 * Ernaehrungsverhaeltnis:
		 * 
		 * 			Ist-Gramm der pflanzlichen Lebensmittel
		 *       	---------------------------------------
		 * 			 Ist-Gramm der tierischen Lebensmittel
		 * res = -----------------------------------------------
		 * 			Soll-Gramm der pflanzlichen Lebensmittel 
		 * 			----------------------------------------
		 * 			 Soll-Gramm der tierischen Lebensmittel
		 * 
		 * Erlaeuterung:
		 * Laut DGE soll der Tagesbedarf der Lebensmittel zu 75 % aus pflanzlichen Lebensmitteln bestehen.
		 * Bei einer Uebereinstimmung von 100 % erhaelt man den Maximalwert. Es handelt sich folglich um ein rein quantitatives Mass.
		 */
		// Klassifikation in Punkteskala 1 bis 10. (Blumthaler S. 91); je hoeher die Punktzahl desto groesser die Uebereinstimmung mit den DGE-Empfehlungen.
		// Vorgaben: // TODO aus Konfigurationsdatei holen
		Double targetWeightAnimalRatio = 0.25;
		Double targetWeightVegetableRatio = 0.75;
		Double sumOfAnimalWeight = 0.0;
		Double sumOfVegetableWeight = 0.0;
		for (FoodItem item : meal.getFoodItems()){
			Double foodItemRatio = item.getRatioAnimalVegetable();
			Double weightOfItem = item.getWeight();
			// TODO integrate FoodUnit
			Double realWeightValue = weightOfItem;
			Double realWeightAnimal = weightOfItem * (foodItemRatio / 100.0); 
			Double realWeightVegetable = weightOfItem * (100.0 - foodItemRatio);
			sumOfAnimalWeight += realWeightAnimal;
			sumOfVegetableWeight += realWeightVegetable; 
		}
		// Bruch berechnen
		Double numerator = 0.0; // Zaehler
		Double denominator = 0.0; // Nenner
		numerator = sumOfVegetableWeight / sumOfAnimalWeight;
		denominator = targetWeightVegetableRatio / targetWeightAnimalRatio;
		Double overallRatio = numerator / denominator;
		int res = -1;
		if (overallRatio >= 90.0){ res = 10;
		} else if (overallRatio >= 80.0){ res = 9;
		} else if (overallRatio >= 70.0){ res = 8;
		} else if (overallRatio >= 60.0){ res = 7;
		} else if (overallRatio >= 50.0){ res = 6;
		} else if (overallRatio >= 40.0){ res = 5;
		} else if (overallRatio >= 30.0){ res = 4;
		} else if (overallRatio >= 20.0){ res = 3;
		} else if (overallRatio >= 10.0){ res = 2;
		} else if (overallRatio >= 00.0){ res = 1;
		} else {
			// TODO throw Exception
		}
		return res;
	}

}
