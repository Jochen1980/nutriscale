package nutriscale.model;

import java.util.List;

/*
 * Class maps different FoodItemGroups of the pyramid to a score. 
 */
public class FoodItemGroup {

	private FoodItemGroupId id;
	private int score; // DGE Pyramidenwert
	
	public FoodItemGroup(FoodItemGroupId id, int score){
		this.id = id;
		this.score = score;
	}
	
	public FoodItemGroup(FoodItemGroupId id, DgePyramid pyramid){
		this.id = id;
		
		// look for already existing score
		List<FoodItemGroup> groups = pyramid.getFoodItemGroups();
		for (FoodItemGroup group : groups){
			if (group.getId() == this.id){
				this.score = group.getScore();
			}
		}
	}
	
	public int getScore(){
		return this.score;
	}
	
	public FoodItemGroupId getId(){
		return this.id;
	}
	
}
