package nutriscale.model;

import java.util.ArrayList;
import java.util.List;

/*
 * Nutrition pyramid.
 */
public class DgePyramid {

	private List<DgePyramidSide> sides;
	private List<FoodItemGroup> groups;
	
	public DgePyramid(){
		// Groups of pyramid.
		FoodItemGroup knabbereien = new FoodItemGroup(FoodItemGroupId.KNABBEREIEN, 1);
		FoodItemGroup getreideProdukte = new FoodItemGroup(FoodItemGroupId.GETREIDEPRODUKTE, 4);
		FoodItemGroup kartoffeln = new FoodItemGroup(FoodItemGroupId.KARTOFFELN, 5);
		FoodItemGroup vollkornProdukte = new FoodItemGroup(FoodItemGroupId.VOLLKORNPRODUKTE, 7);
		FoodItemGroup gemueseUndObst = new FoodItemGroup(FoodItemGroupId.GEMUESEUNDOBST, 9);
		FoodItemGroup speck = new FoodItemGroup(FoodItemGroupId.SPECK, 1);
		FoodItemGroup fettreicheFleischwaren = new FoodItemGroup(FoodItemGroupId.FETTREICHEFLEISCHWAREN, 3);
		FoodItemGroup fettreichereMilchprodukte = new FoodItemGroup(FoodItemGroupId.FETTREICHEMILCHPRODUKTE, 5);
		FoodItemGroup fettarmeMilchprodukte = new FoodItemGroup(FoodItemGroupId.FETTARMEMILCHPRODUKTE, 6);
		FoodItemGroup fettarmeFleischwaren = new FoodItemGroup(FoodItemGroupId.FETTARMEFLEISCHWAREN, 9);
		FoodItemGroup fisch = new FoodItemGroup(FoodItemGroupId.FISCH, 10);
		FoodItemGroup schmalz = new FoodItemGroup(FoodItemGroupId.SCHMALZ, 1);
		FoodItemGroup butter = new FoodItemGroup(FoodItemGroupId.BUTTER, 2);
		FoodItemGroup margarine = new FoodItemGroup(FoodItemGroupId.MARGARINE, 3);
		FoodItemGroup sonnenblumenoel = new FoodItemGroup(FoodItemGroupId.MAISKEIMUNDSONNENBLUMENOEL, 5);
		FoodItemGroup olivenoel = new FoodItemGroup(FoodItemGroupId.WEIZENKEIMSOJAUNDOLIVENOEL, 6);
		FoodItemGroup rapsoel = new FoodItemGroup(FoodItemGroupId.RAPSUNDWALNUSSOEL, 9);
		FoodItemGroup limonadenEnergyDrinks = new FoodItemGroup(FoodItemGroupId.LIMONADENUNDENERGYDRINKS, 1);
		FoodItemGroup fruchtsaftschorlen = new FoodItemGroup(FoodItemGroupId.FRUCHTSAFTSCHORLENUNDLIGHTGETRAENKE, 4);
		FoodItemGroup gruenerTeeKaffee = new FoodItemGroup(FoodItemGroupId.GRUENERTEEUNDKAFFEE, 7);
		FoodItemGroup fruechteTee = new FoodItemGroup(FoodItemGroupId.KRAEUTERUNDFRUECHTETEE, 9);
		FoodItemGroup wasser = new FoodItemGroup(FoodItemGroupId.TRINKUNDMINERALWASSER, 10);
		// create pyramid
		ArrayList<FoodItemGroup> pflanzlicheLmGroups = new ArrayList<FoodItemGroup>();
		pflanzlicheLmGroups.add(knabbereien);
		pflanzlicheLmGroups.add(getreideProdukte);
		pflanzlicheLmGroups.add(kartoffeln);
		pflanzlicheLmGroups.add(vollkornProdukte);
		pflanzlicheLmGroups.add(gemueseUndObst);
		ArrayList<FoodItemGroup> tierischeLmGroups = new ArrayList<FoodItemGroup>();
		tierischeLmGroups.add(speck);
		tierischeLmGroups.add(fettreicheFleischwaren);
		tierischeLmGroups.add(fettreichereMilchprodukte);
		tierischeLmGroups.add(fettarmeMilchprodukte);
		tierischeLmGroups.add(fettarmeFleischwaren);
		tierischeLmGroups.add(fisch);
		ArrayList<FoodItemGroup> oeleUndFetteGroups = new ArrayList<FoodItemGroup>();
		oeleUndFetteGroups.add(schmalz);
		oeleUndFetteGroups.add(butter);
		oeleUndFetteGroups.add(margarine);
		oeleUndFetteGroups.add(sonnenblumenoel);
		oeleUndFetteGroups.add(olivenoel);
		oeleUndFetteGroups.add(rapsoel);
		ArrayList<FoodItemGroup> getraenkeGroups = new ArrayList<FoodItemGroup>();
		getraenkeGroups.add(limonadenEnergyDrinks);
		getraenkeGroups.add(fruchtsaftschorlen);
		getraenkeGroups.add(gruenerTeeKaffee);
		getraenkeGroups.add(fruechteTee);
		getraenkeGroups.add(wasser);
		// TODO consider using interfaces.
		DgePyramidSide pflanzlicheLm = new DgePyramidSide("Pflanzliche Lebensmittel", pflanzlicheLmGroups);
		DgePyramidSide tierischeLm = new DgePyramidSide("Tierische Lebensmittel", tierischeLmGroups);
		DgePyramidSide oeleUndFette = new DgePyramidSide("Öle und Fette", oeleUndFetteGroups);
		DgePyramidSide getraenke = new DgePyramidSide("Getränke", getraenkeGroups);
		sides = new ArrayList<DgePyramidSide>();
		sides.add(oeleUndFette);
		sides.add(pflanzlicheLm);
		sides.add(tierischeLm);
		sides.add(getraenke);
	}
	
	/*
	 * Method for iterating all groups to detect to corresponding score.
	 */
	public List<FoodItemGroup> getFoodItemGroups(){
		ArrayList<FoodItemGroup> groups = new ArrayList<FoodItemGroup>();
		for (DgePyramidSide side : this.sides){
			List<FoodItemGroup> groupsOfCurrentSide = side.getGroups();
			groups.addAll(groupsOfCurrentSide);
		}
		return groups;
	}
	
}
