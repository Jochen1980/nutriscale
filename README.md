# NutriScale – Kennzahlen zur Bewertung der Nahrungsmittelauswahl

*Jochen Bauer, Markus Michl, Christina Blumthaler, Sergej Wiebe, Ria Rashid, Jörg Franke*

*E|Home-Center, Lehrstuhl für Fertigungsautomatisierung und Produktionssystematik, Friedrich-Alexander-Universität Erlangen-Nürnberg*

## Hintergrund
Menschen schätzen Orientierung bei Fragen der Lebensmittelauswahl, insbesondere wenn ernährungsassoziierte Krankheiten vorliegen. Anbieter von Mobiltelefon-Apps beabsichtigen zu dieser Orientierung beizutragen. Die Apps eignen sich zur Datenerfassung von Speisen, es fehlt hingegen an Algorithmen, die den Verzehr bewerten – der hier entwickelte NutriScale-Algorithmus schließt diese Lücke.

## Methoden
Diverse Apps zur Erfassung von Ernährungsgewohnheiten wurden analysiert und notwendige Funktionen für eine nutzergerechte Eingabe abgeleitet. Es folgte ein Funktionalitätsvergleich zwischen aktuellen Apps und Fachsoftware für Ernährungstherapeuten. Des Weiteren wurden  verfügbare Orientierungshilfen für den Transfer einer gesunden Ernährungsweise in den Alltag evaluiert. Auf Basis dieser Analysen wurde eine Softwareanwendung konzeptioniert, welche Lebensmittel nutzergerecht erfasst und hilfreiche Bewertungen zur Qualität der Nahrungszusammenstellung bietet.

## Ergebnisse
Die App „Jawbone Up“ erlaubt eine nutzergerechte Erfassung der Ernährungsinformation. Das Modell der DGE-Lebensmittelpyramide eignet sich als Grundlage für eine automatisierte Bewertung der Lebensmittel und somit als Basis von NutriScale.

NutriScale besteht aus zwei Kennzahlen: Nutri-Scale-Ratio α (NSR, siehe Formel 1) und NutriScale-Score β (NSS, siehe Formel 2). NSS berücksichtigt die auf den vier Seiten der Pyramide abgebildeten Lebensmittelgruppen. Jeder Gruppe wird ein Punktewert zwischen 1 (oberste Gruppe) und 10 (unterste Gruppe) zugeordnet. NSR lehnt sich an den DGE-Ernährungskreis mit dessen anvisiertem Verhältnis von pflanzlichen zu tierischen Lebensmitteln, 3:1, an.

![Formeln, Ernährungsverhältnis, NSR und Ernährungspunktzahl, NSS](https://bytebucket.org/Jochen1980/nutriscale/raw/7069591f6f74b0f61394be2c49fb4fdfd18816df/resources/AbstractPaperWk53_NutriScale-final_formeln.png)

## Schlussfolgerung
NutriScale wird in Form eines Webservices verfügbar gemacht. Bestehende Apps können folglich die Kennzahlen in ihren Apps einbinden und so die eigene App bereichern. 

NutriScale-Software-Projekt: https://bitbucket.org/Jochen1980/nutriscale